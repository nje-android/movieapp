package gamf.movieapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gamf.movieapp.model.Movie;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieHolder> {

    private MovieListFragment movieListFragment;
    private List<Movie> movies;


    public MovieListAdapter(MovieListFragment movieListFragment, List<Movie> movies) {
        this.movieListFragment = movieListFragment;
        this.movies = movies;
    }

    @Override
    public MovieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_item, parent, false);

        return new MovieHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovieHolder holder, int position) {
        final Movie movie = movies.get(position);
        holder.titleTextView.setText(movie.getTitle());
        holder.genreTextView.setText(movie.getGenre());
        holder.yearTextView.setText(Long.toString(movie.getYear()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movieListFragment.selectItemAndNavigate(movie);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class MovieHolder extends RecyclerView.ViewHolder {
        public View itemView;
        public TextView titleTextView;
        public TextView genreTextView;
        public TextView yearTextView;

        public MovieHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            titleTextView = itemView.findViewById(R.id.TitleTextView);
            genreTextView = itemView.findViewById(R.id.GenreTextView);
            yearTextView = itemView.findViewById(R.id.YearTextView);
        }
    }
}
