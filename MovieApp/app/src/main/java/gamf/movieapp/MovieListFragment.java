package gamf.movieapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import gamf.movieapp.model.Movie;
import gamf.movieapp.model.MovieManager;

public class MovieListFragment extends Fragment {

    public MovieListFragment() {
    }

    public static MovieListFragment newInstance() {
        MovieListFragment fragment = new MovieListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_list, container, false);

        // create empty list for adapter
        MovieManager movieManager = ((MainActivity)getActivity()).getMovieManager();
        List<Movie> movies = movieManager.getMovies();

        RecyclerView recyclerView = rootView.findViewById(R.id.MovieRecyclerView);

        MovieListAdapter adapter = new MovieListAdapter(this, movies);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(getActivity()
                        .getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    public void selectItemAndNavigate(Movie movie) {
        ((MainActivity) getActivity()).navigateToDetails(movie);
    }
}
