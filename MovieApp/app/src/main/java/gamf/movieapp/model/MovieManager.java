package gamf.movieapp.model;

import java.util.ArrayList;
import java.util.List;

public class MovieManager {

    private List<Movie> movies;

    public MovieManager() {
        movies = new ArrayList<>();

        movies.add(new Movie(1, "The Guilty", "Thriller", 2018));
        movies.add(new Movie(2, "Mission: Impossible - Fallout", "Action", 2018));
        movies.add(new Movie(3, "Deadpool 2", "Action & Adventure", 2018));
        movies.add(new Movie(4, "Hold the Dark", "Adventure & Drama", 2018));
        movies.add(new Movie(5, "Avengers: Infinity War", "Action & Sci-Fi", 2018));
        movies.add(new Movie(6, "Destroyer", "Action & Crime", 2018));
        movies.add(new Movie(7, "Incredibles 2", "Animation & Action", 2018));
        movies.add(new Movie(8, "Captain Marvel", "Action & Adventure", 2019));
        movies.add(new Movie(9, "Pokémon Detective Pikachu", "Action & Adventure", 2019));
        movies.add(new Movie(10, "John Wick: Chapter 3 - Parabellum", "Action & Crime", 2019));
        movies.add(new Movie(11, "Yesterday", "Comedy", 2019));
        movies.add(new Movie(12, "Toy Story 4", "Animation", 2019));
        movies.add(new Movie(13, "Spider-Man: Far from Home", "Action & Adventure", 2019));
        movies.add(new Movie(14, "The Lion King", "Action & Adventure", 2019));
        movies.add(new Movie(15, "Once Upon a Time ... in Hollywood", "Comedy", 2019));
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public Movie getMovieById(long id) {
        Movie movie = null;
        for(Movie m : movies) {
            if(m.getId() == id) {
                movie = m;
            }
        }
        return movie;
    }
}
