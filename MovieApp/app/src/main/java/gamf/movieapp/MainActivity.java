package gamf.movieapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import gamf.movieapp.model.Movie;
import gamf.movieapp.model.MovieManager;

public class MainActivity extends AppCompatActivity {

    private MovieManager movieManager;
    private Movie selectedMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        movieManager = new MovieManager();
        loadFragment(MovieListFragment.newInstance(), "movies", false);
    }

    private void loadFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ContainerFrameLayout, fragment, tag);

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    public MovieManager getMovieManager() {
        return movieManager;
    }

    public void navigateToDetails(Movie movie) {
        selectedMovie = movie;
        loadFragment(MovieDetailsFragment.newInstance(), "details", true);
    }

    public Movie getSelectedMovie() {
        return selectedMovie;
    }
}
