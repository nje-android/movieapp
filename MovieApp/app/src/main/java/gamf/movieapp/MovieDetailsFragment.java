package gamf.movieapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gamf.movieapp.model.Movie;

public class MovieDetailsFragment extends Fragment {

    public MovieDetailsFragment() {
    }

    public static MovieDetailsFragment newInstance() {
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_details, container, false);

        Movie movie = ((MainActivity)getActivity()).getSelectedMovie();

        TextView titleTextView = rootView.findViewById(R.id.TitleTextView);
        titleTextView.setText(movie.getTitle());

        return rootView;
    }
}
